#!/bin/bash
set -euo pipefail
hostname=${1:-}
if [[ -z ${hostname} ]]; then
  echo -n "Enter hostname: "
  read hostname
fi

disk_prefix="/var/lib/libvirt/images/${hostname}"

tempconfig=$(mktemp -td cloudinit.XXXXX)

echo "instance-id: $hostname" >> $tempconfig/meta-data
echo "local-hostname: $hostname" >> $tempconfig/meta-data

cat > $tempconfig/user-data <<EOF
#cloud-config
users:
  - default
  - name: finn
    groups: systemd-journal
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    ssh_authorized_keys:  # These SSH keys will be overwritten by ansible-pull when the VM first boots
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAII0X72+AAidiYPa2ORq1pVk1UTpENVh/aAJ5bNUU74/b finn@plur-police
  - name: devan
    groups: systemd-journal
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    ssh_authorized_keys:  # These SSH keys will be overwritten by ansible-pull when the VM first boots
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILL6PhAcMxsc4GhRuQYRMwchcig5qTDQQFZBQzxFTmxI
package_update: true
packages:
  - ansible
  - git
runcmd:
  - ansible-pull -U "https://git.callpipe.com/entanglement.garden/ansible-basic.git"
  - run-ansible-pull nodisown
power_state:
  mode: reboot
EOF

pushd ${tempconfig}
genisoimage -output "${disk_prefix}-cloudinit.iso" -volid cidata -joliet -r user-data meta-data
popd
rm -rf ${tempconfig}

cp /var/installmedia/debian-10-openstack-amd64.qcow2 "${disk_prefix}.qcow2"

qemu-img resize "${disk_prefix}.qcow2" 20G

virt-install --rng /dev/random --connect qemu:///system --virt-type=kvm --import --name "${hostname}" --ram 1024 --vcpus 1 --disk "${disk_prefix}.qcow2,format=qcow2,bus=virtio" --disk "${disk_prefix}-cloudinit.iso,device=cdrom" --network bridge=brlan,model=virtio --console pty,target_type=serial --os-type=linux --os-variant=debian9 --noautoconsole
