#!/bin/bash
set -euo pipefail
VM_IMAGES_PATH="/var/lib/libvirt/images"
BASE_VM_IMAGE="$VM_IMAGES_PATH/gitlab-job.qcow2"
VM_ID="runner-$CUSTOM_ENV_CI_RUNNER_ID-project-$CUSTOM_ENV_CI_PROJECT_ID-concurrent-$CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID-job-$CUSTOM_ENV_CI_JOB_ID"
VM_IMAGE="$VM_IMAGES_PATH/$VM_ID.qcow2"
VM_HOSTNAME="runner-${CUSTOM_ENV_CI_JOB_ID}"
export LIBVIRT_DEFAULT_URI="qemu:///system"

_get_vm_ip() {
    virsh -q domifaddr --source agent "$VM_ID" 2> /dev/null | awk '{print $4}' | sed -E 's|/([0-9]+)?$||' | grep '172.18' || true
}
