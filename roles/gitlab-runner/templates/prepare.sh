{% include 'base.sh' %}

# trap any error, and mark it as a system failure.
trap "exit $SYSTEM_FAILURE_EXIT_CODE" ERR

pushd /var/lib/libvirt/images
if ! curl -s http://image-host/gitlab-job/SHA256SUMS | grep -E "gitlab-job.qcow2$" | sha256sum -c ; then
    wget -qO gitlab-job.qcow2.xz http://image-host/gitlab-job/gitlab-job.qcow2.xz
    curl -s http://image-host/gitlab-job/SHA256SUMS | grep -E "gitlab-job.qcow2.xz$" | sha256sum -c
    rm gitlab-job.qcow2 || true
    unxz -v gitlab-job.qcow2.xz
fi
popd

# Copy base disk to use for Job.
cp "$BASE_VM_IMAGE" "$VM_IMAGE"

tempconfig=$(mktemp -td cloudinit.XXXXX)
echo "instance-id: ${VM_HOSTNAME}" >> $tempconfig/meta-data
echo "local-hostname: ${VM_HOSTNAME}" >> $tempconfig/meta-data

ssh-keygen -t ed25519 -f /tmp/${VM_ID}_ed25519 -qN ""

cat > $tempconfig/user-data <<EOF
#cloud-config
ssh_authorized_keys:
  - $(cat /tmp/${VM_ID}_ed25519.pub)
EOF

genisoimage -output "${VM_IMAGES_PATH}/${VM_ID}-cloudinit.iso" -volid cidata -joliet -r "${tempconfig}"


# Install the VM
virt-install \
    --name "$VM_ID" \
    --os-variant debian10 \
    --disk "$VM_IMAGE" \
    --disk "${VM_IMAGES_PATH}/${VM_ID}-cloudinit.iso,device=cdrom" \
    --import \
    --vcpus=2 \
    --ram=2048 \
    --network bridge=brlan,model=virtio \
    --graphics none \
    --noautoconsole

# Wait for VM to get IP
echo 'Waiting for VM to get IP'
for i in $(seq 1 300); do
    VM_IP=$(_get_vm_ip)

    if [ -n "$VM_IP" ]; then
        echo "VM got IP: $VM_IP"
        break
    fi

    if [ "$i" == "300" ]; then
        echo 'Waited 5 minutes for VM to start, exiting...'
        # Inform GitLab Runner that this is a system failure, so it
        # should be retried.
        exit "$SYSTEM_FAILURE_EXIT_CODE"
    fi

    sleep 1
done

ssh-keygen -R "$VM_IP" || true

# Wait for ssh to become available
echo "Waiting for sshd to be available"
for i in $(seq 1 300); do
    if ssh -i /tmp/${VM_ID}_ed25519 -o StrictHostKeyChecking=no debian@"$VM_IP" >/dev/null 2>/dev/null; then
        break
    fi

    if [ "$i" == "300" ]; then
        echo 'Waited 5 minutes for sshd to start, exiting...'
        # Inform GitLab Runner that this is a system failure, so it
        # should be retried.
        exit "$SYSTEM_FAILURE_EXIT_CODE"
    fi

    sleep 1
done

rm -rf "$tempconfig"
